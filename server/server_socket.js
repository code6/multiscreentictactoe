var _ = require('underscore');

function isAlphaNumeric(string){
    return !/[^a-zA-Z0-9]/.test( string );
}

var playersGlobal = {}; // like a User Account
var nextUserID = 1000;

var Game = function(roomCode){
    var players = []; // Game Players

    var gameState = { // TODO should be filled in from Game class (later various types?)
        phase: 'preGame',
        curTurnPlayerId: null,
        victorUserId: null,
        board: _.map(_.range(3),function(){return _.map(_.range(3),function(){return '';});}) // 3x3 array of ''
    };

    var findPlayer = function(userID){
        return _.findWhere(players, {userID: userID});
    };

    // Player is stripped of all private game state data, for sending to players
    var getPlayerDataPublic = function(player){
        var playerOut = _.pick(player,'userID','gamePiece'); //remove all keys but these
        _.extend(playerOut,{
            name: playersGlobal[player.userID].name,
            online: playersGlobal[player.userID].online
        });
        return playerOut;
    };

    var getPlayersDataPublicForRoom = function(){
        return  _.map(players, function(player){
            return getPlayerDataPublic(player);
        });
    };

    var socketJoin = function(socket, reply){
        var userID = socket.data.userID;
        var isPlayer = false;

        // Re-establish new guy as player, if so
        var returningPlayer = findPlayer(userID);
        // userID not in player list, or already have online player with that userID
        if(returningPlayer){ // old player
            isPlayer = true;
            console.log(socket.id+' returns as old player '+userID+' in: '+roomCode);

            roomManager.getRoom(roomCode).roomEmitGameEventPublic('setPlayers', {players: getPlayersDataPublicForRoom()});
        }

        // new spectators get all public info on player game state
        reply({success: true, isPlayer: isPlayer, players: getPlayersDataPublicForRoom(), gameState: gameState});
    };

    var becomePlayer = function(socket, reply){
        var returningPlayer = findPlayer(socket.data.userID);
        // userID not in player list, or already have online player with that userID
        if(!returningPlayer){ // new player
            if(gameState.phase !== 'preGame'){
                console.log(socket.id+' join fail: not at preGame');
                reply({success: false});
                return false;
            }else if(players.length >= 2){
                console.log(socket.id+' join fail: too many players');
                reply({success: false});
            }
            // New player - assign a new userID, add to players
            console.log(socket.id+' becomes a new player '+socket.data.userID+' in: '+roomCode);
            var newPlayerObject ={
                userID: socket.data.userID,
                socket: socket
            };
            players.push(newPlayerObject);

            // Tell everyone (incl new player) about new player
            roomManager.getRoom(roomCode).roomEmitGameEventPublic('setPlayers', {players: getPlayersDataPublicForRoom()});

            //TODO Private player data?
            reply({success: true}); // by the time player knowns he is a player, he already knowns that he joined
            checkIfEnoughPlayersToStart();

        }else{ // Old Player - already a player - why again?
            console.log(socket.id+' become player fail: already playing');
            reply({success: false});
            return false;
        }
    };

    var checkIfEnoughPlayersToStart = function(){
        if(gameState.phase === 'preGame' && players.length === 2){
            gameState.phase = 'game';
            gameState.curTurnPlayerId = players[0].userID;
            players[0].gamePiece = 'x';
            players[1].gamePiece = 'o';
            console.log('starting game');
            roomManager.getRoom(roomCode).roomEmitGameEventPublic('setGameState', {gameState: gameState}); //TODO so common, this should be a param-less function
            roomManager.getRoom(roomCode).roomEmitGameEventPublic('setPlayers', {players: getPlayersDataPublicForRoom()});
        }else{
            console.log('not proceeding to game phase');
        }
    };

    var makeMove = function(socket, data, reply){
        if(socket.data.userID === gameState.curTurnPlayerId){
            console.log(socket.data.userID+' makeMove: '+data.indexRow+','+data.indexCell);
            gameState.board[data.indexRow][data.indexCell] = findPlayer(socket.data.userID).gamePiece;

            if(!isGameOver()){
                var curPlayer = findPlayer(gameState.curTurnPlayerId);
                var nextPlayer = _.without(players, curPlayer)[0];
                gameState.curTurnPlayerId = nextPlayer.userID;
            }

            roomManager.getRoom(roomCode).roomEmitGameEventPublic('setGameState', {gameState: gameState});
        }
    };

    var isGameOver = function(){
        var board = gameState.board;
        var gameIsOver = false;

        var thereIsAWinner = _.any([
            _.any(board, function(row){ // left to right
                return !!row[0] && row[0] === row[1] && row[1] === row[2];
            }),
            _.any(_.range(board[0].length), function(cel){ // up to down
                return !!board[0][cel] && board[0][cel] === board[1][cel] && board[1][cel] === board[2][cel];
            }),
            (!!board[0][0] && board[0][0] === board[1][1] && board[1][1] === board[2][2]), // topLeft to bottomRight
            (!!board[0][2] && board[0][2] === board[1][1] && board[1][1] === board[2][0]) // topRight to bottomLeft
        ],function(f){return f;});
        if(thereIsAWinner){
            console.log('gameOver');
            gameIsOver = true;
            gameState.victorUserId = gameState.curTurnPlayerId; // must have just played to win
            gameState.curTurnPlayerId = null;
        }else{
            var isTie = _.all(board, function(row){ // No winner and all fields in use
                return _.all(row, function(cel){
                    return !!cel;
                });
            });
            if(isTie){
                console.log('gameOver: tie');
                gameIsOver = true;
                gameState.victorUserId = null;
                gameState.curTurnPlayerId = null;
            }
        }
        if(gameIsOver){
            gameState.phase = 'gameOver';
            return true;
        }
        return false;
    };

    return {
        getPlayersDataPublicForRoom: getPlayersDataPublicForRoom, //todo remove
        socketJoin: socketJoin,
        becomePlayer: becomePlayer,
        makeMove: makeMove
    };
};

var Room = function(roomCode){
    var sockets = [];

    var game = new Game(roomCode);

    var getGame = function(){return game;};
    var getRoomCode = function(){return roomCode;};

    var roomEmit = function(type, data){ //TODO Only called once - what is it good for?
        _.each(sockets,function(socket){
            socket.emit(type, data);
        });
    };

    // For public game events. userID optional, and injected into data as 'userID' (by convention for any pub event relating to a specific player, like isReady)
    var roomEmitGameEventPublic = function(type, data, userID){ // this is good here, but standardize
        var outData ={type:type,data:data};
        if(userID) { outData.userID=userID; }
        _.each(sockets,function(socket){
            socket.emit('public', outData);
        });
    };

    var roomJoin = function(socket, reply){
        socket.data.room = roomCode;
        console.log(socket.id+' joining: '+roomCode);
        sockets.push(socket);
        console.log('in room: '+sockets.length);

        game.socketJoin(socket, reply);
    };

    var roomLeave = function(socket){
        sockets = _.without(sockets, socket);
        console.log(socket.id+' leaves', roomCode);

        roomEmitGameEventPublic('setPlayers', {players: getGame().getPlayersDataPublicForRoom()});
    };

    return{
        getRoomCode: getRoomCode,
        getGame: getGame,
        roomEmit: roomEmit,
        roomEmitGameEventPublic: roomEmitGameEventPublic,
        roomJoin: roomJoin,
        roomLeave: roomLeave
    };
};

var RoomManager = (function () {
    var rooms = {};

    var genCode = function() {
        var digit = function() {
            return Math.floor(Math.random()*10);
        };
        return _.chain(_.range(7)).map(function(c){return digit();}).value().join('');
    };

    var roomCreate = function(){
        var roomCode;
        do { // ensure that it doesn't exist yet
            roomCode = genCode();
        } while (!!rooms[roomCode]);

        rooms[roomCode]= new Room(roomCode);
        return roomCode;
    };

    // socket/spectator (not player) leave whatever room this user was in, if any (otherwise NOP)
    var roomLeave = function(socket){
        var roomCode = socket.data.room;
        if(roomCode){
            getRoom(roomCode).roomLeave(socket);
            socket.data.room = null;
        }
    };

    // everyone initially joins a room as a spectator and might become a player later
    var roomJoin = function(socket, roomCode, reply){
        //Kick user out of a potential previous room
        roomLeave(socket);
        // Err if no such room
        if(!getRoom(roomCode)){
            console.log(socket.id+' join fail: '+roomCode);
            reply({success: false});
        }else{
            getRoom(roomCode).roomJoin(socket, reply);
        }
    };

    var getRoom = function(roomCode){
        return rooms[roomCode];
    };

    return {
        roomCreate: roomCreate,
        roomJoin: roomJoin,
        roomLeave: roomLeave,
        getRoom: getRoom
    };
});
var roomManager = new RoomManager();


var socket = function(socket) {
    if(socket.data){
        console.log('ERROR: SocketIO uses socket.data now?');
    }else{
        socket.data={};
    }

    socket.on('login', function(data, reply) {
        var userID = data.userID;
        console.log(socket.id,'wants to login as',userID);
        if(playersGlobal[userID] && !playersGlobal[userID].online){ // Login ok (Existing user and not already online)
            console.log(socket.id+' logs in as '+userID);
            socket.data.userID = userID;
            playersGlobal[userID].online=true;
        }else{
            // New user - assign a new userID, add to players
            console.log(socket.id+' becomes a new user '+nextUserID);
            var newPlayerObject ={
                userID: nextUserID,
                socket: socket
            };
            playersGlobal[nextUserID]={
                name: 'Guest' + nextUserID,
                online: true
            };
            socket.data.userID = nextUserID;
            nextUserID = nextUserID + 1;
        }

        reply({success: true, userID: socket.data.userID});
    });

    socket.on('roomCreate', function(data, reply) {
        if(!socket.data.userID){ // logged in check
            reply({success: false});
            return;
        }
        reply({success: true, roomCode: roomManager.roomCreate()});
    });

    socket.on('roomJoin', function(data, reply) {
        if(!socket.data.userID){ // logged in check
            reply({success: false});
            return;
        }
        roomManager.roomJoin(socket, data.roomCode, reply);
    });

    socket.on('roomLeave', function(data, reply) {
        console.log(socket.id,'wants to leave room');
        roomManager.roomLeave(socket);
        reply({success: true});
    });

    // spectator socket wants to play (doubles as a semi-login)
    socket.on('becomePlayer', function(data, reply) {
        var roomCode = socket.data.room;
        if(!roomCode){
            console.log(socket.id+' become player fail: Not in a room');
            reply({success: false});
            return;
        }else{
            roomManager.getRoom(roomCode).getGame().becomePlayer(socket, reply);
        }
    });

    socket.on('makeMove', function(data, reply) {
        var roomCode = socket.data.room;
        if(!roomCode){
            console.log(socket.id+' makeMove fail: Not in a room');
            reply({success: false});
            return;
        }else{
            roomManager.getRoom(roomCode).getGame().makeMove(socket, data, reply);
        }
    });

    socket.on('disconnect', function () {
        // if this is a logged in user, set as offline
        if(socket.data.userID) { playersGlobal[socket.data.userID].online = false; }
        roomManager.roomLeave(socket);
        console.log(socket.id+' disconnects');
    });
};

module.exports = socket;