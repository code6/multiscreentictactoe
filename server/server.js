// Server
var _ = require('underscore');
var io = require('socket.io').listen(8066);
var socket = require('./server_socket.js');

io.configure(function(){ //https://github.com/LearnBoost/Socket.IO/wiki/Configuring-Socket.IO
    io.set('log level', 1);
});

io.sockets.on('connection', socket);
