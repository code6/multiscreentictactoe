'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', ['ui','myApp.filters', 'myApp.services', 'myApp.directives']).
  config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/home', {templateUrl: 'partials/home.html', controller: HomeCtrl});
    $routeProvider.when('/about', {templateUrl: 'partials/about.html', controller: AboutCtrl});
    $routeProvider.when('/room/:roomId', {templateUrl: 'partials/room.html', controller: RoomCtrl});
    $routeProvider.otherwise({redirectTo: '/home'});
  }]);
