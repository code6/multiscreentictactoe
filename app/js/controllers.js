'use strict';

/* Controllers */


function HomeCtrl($scope, $window, socket) {
    $scope.socket = socket;
//    $scope.socket.roomLeave();

    $scope.roomCode = '';
    $scope.clickJoin = function(){
        $scope.socket.roomJoin($scope.roomCode);
    };
    $scope.clickCreate = function(){
        $scope.socket.roomCreate();
    };
    $scope.reconnect = function(){
        return $window.location.reload();
    };
}
//HomeCtrl.$inject = []; // Required for minify


function RoomCtrl($scope, $routeParams, $location, $window, socket) {
    $scope.socket = socket;
    console.log($routeParams.roomId);
    $scope.socket.roomJoin($routeParams.roomId);

    $scope.urlToJoin = function(){
        return $location.absUrl();
    };
    $scope.reconnect = function(){
        return $window.location.reload();
    };
}
//RoomCtrl.$inject = [];

function TicTacToeCtrl($scope, socket) {
    $scope.socket = socket;
    $scope.wantsToBePlayer = false;

    $scope.becomePlayer = function(){
        $scope.wantsToBePlayer = true;
        $scope.socket.becomePlayer();
    };
    $scope.makeMove = function(indexRow, indexCell){
        console.log('makeMove '+indexRow+', '+indexCell);
        $scope.socket.makeMove(indexRow, indexCell); //ToDo standardize into subtype of generic gameMsg
    };
}
//RoomCtrl.$inject = [];

function AboutCtrl($scope) {
}
//AboutCtrl.$inject = [];
