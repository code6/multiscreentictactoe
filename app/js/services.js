'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('myApp.services', []).
    value('version', '0.1').
    factory('socket', function ($rootScope, $location) { //FIXME Uppercase by convention
        var socket = io.connect('http://'+window.location.hostname+':8066', {
//        var socket = io.connect('http://dev.puchisoft.com:8066', {
            reconnect: false
        });
        var gameStateDefault = { //TODO Move to TicTacToeCtrl
            phase: 'preGame',
            curTurnPlayerId: null,
            board: []
        };
        var stateDefault = {
            isConnected: false,
            hasLostConnection: false,
            roomCode: null,
            roomCodeDesired: null,
            isPlayer: false, //TODO Move to TicTacToeCtrl
            userID: null,
            gameState: {}, //TODO Move to TicTacToeCtrl
            players: [] //TODO Move to TicTacToeCtrl
        };
        var state = _.extend({}, stateDefault);

        var resetRoom = function(){
            state.isPlayer = false;
            state.roomCode = null;
            state.roomCodeDesired = null;
            state.userID = null;
            state.gameState = _.extend({}, gameStateDefault);
            state.players = [];
        };
        resetRoom();

        var on = function (eventName, callback) {
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () { // causes Angular to refresh dom listeners after execution completes
                    callback.apply(socket, args);
                });
            });
        };

        var emit = function (eventName, data, callback) {
            socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            });
        };

        on('connect',function(){
//            state = _.extend({}, stateDefault); // reset game state to knowing nothing // Issue: Connection can happen after a Room Join, so reset here is bad
            state.isConnected = true;
            login();
        });
        on('disconnect',function(){
            state.isConnected = false;
            state.hasLostConnection = true;
            state.userID = null;
        });

        var login = function(){
            emit('login', {userID: store.get('userID')}, function(data){
                if(data.success){
                    state.userID = data.userID;
                    store.set('userID',data.userID);
                    console.log('logged in as: '+data.userID);
                    roomJoinActual();
                }else{
                    console.log('login fail');
                }
            });
        };
        var isLoggedIn = function(){ return !!state.userID;};

        // Join desired room - called either as soon as the user wants if he is already logged in, or after login success (if user wanted to join instantly but wasn't logged in yet)
        var roomJoinActual = function(){
            if(!state.roomCodeDesired || state.roomCode === state.roomCodeDesired){return;} // NOP if already in this room
            emit('roomJoin', {roomCode: state.roomCodeDesired},function(data){
                if(data.success){
                    state.roomCode = state.roomCodeDesired;
                    state.userID = store.get('userID');
                    state.isPlayer = data.isPlayer;
                    state.players = data.players;
                    state.gameState = data.gameState;
                    console.log('join ok');
                    console.log(data);
                    $location.url('room/'+state.roomCode);
                }else{
                    console.log('join fail');
                    $location.url('home');
                }
            });
        };

        on('public', _.bind(function (data) {
            onGameEventPublic(data.type, data.data, data.userID);
        },this));

        var findPlayer = function(userID){
            return _.findWhere(state.players, {userID: userID});
        };

        //TODO Move to TicTacToeCtrl
        // For public gameEvents (userID is optional)
        var onGameEventPublic = function(type, data, userID){
            var player = findPlayer(userID); //only for some messages

            console.log('socket public event: '+type);
            console.log(data);
            switch(type){
                case 'setName':
                    player.name = data.name;
                    return;
                case 'setPhase':
                    state.gameState.phase = data.phase;
                    return;
                case 'setPlayers':
                    state.players = data.players;
                    return;
                case 'setGameState':
                    state.gameState = data.gameState;
                    return;
                default:
                    alert('onGameEventPublic unknown type: '+type);
            }
        };

        return {
            // Pass-through
            on: on,
            emit: emit,

            // App-Specific
            state: state,
            isLoggedIn: isLoggedIn,
            roomCreate: function(){
                this.emit('roomCreate', {}, _.bind(function (data) { // socket param before callback param (same on server)
                    if(data.success){
                        console.log('create ok: '+data.roomCode);
                        this.roomJoin(data.roomCode);
                    }else{
                        console.log('failed to create');
//                        $location.url('home');
                    }
                },this));
            },
            roomJoin: function(roomCode){
                state.roomCodeDesired = roomCode;
                if(isLoggedIn()){
                    roomJoinActual();
                }
            },
            roomLeave: function(roomCode){
                this.emit('roomLeave', {},function(data){
                    if(data.success){
                        resetRoom();
                    }
                });
            },
            becomePlayer: function(){ //TODO Move to TicTacToeCtrl
                this.emit('becomePlayer', {}, _.bind(function(data){
                    if(data.success){
                        state.isPlayer = true;
                        console.log('became player');
                        // TODO: all of my private state should be returned here
                    }else{
                        console.log('became player fail');
                    }
                },this));
            },
            getConnectionState: function(){
                if (state.isConnected ){
                    if(!!state.roomCode){
                        return "InRoom";
                    }else{
                        return "Connected";
                    }
                }else if(state.hasLostConnection){
                    return "LostConnection";
                }else{
                    return "Connecting";
                }
            },
            currentPlayerName: function(){ //TODO Move to TicTacToeCtrl
                return findPlayer(state.gameState.curTurnPlayerId).name;
            },
            getVictorName: function(){ //TODO Move to TicTacToeCtrl
                var player = findPlayer(state.gameState.victorUserId)
                return player ? player.name : '';
            },
            isPlayerMe: function(player){ //TODO Move to TicTacToeCtrl
                return state.userID === player.userID;
            },
            isMyTurn: function(){ //TODO Move to TicTacToeCtrl
                return state.gameState.curTurnPlayerId === state.userID;
            },
            makeMove: function(indexRow, indexCell){ //TODO Move to TicTacToeCtrl
                if(!this.isMyTurn){ return; }
                this.emit('makeMove', {indexRow:indexRow, indexCell:indexCell}, _.bind(function(data){
                    if(!data.success){
                        console.log('makeMove fail');
                    }
                },this));
            }
        };
    });

