# MultiScreenTicTacToe

MultiScreenTicTacToe uses Angular, Bootstrap, NodeJS, and Socket.IO. It's based on the angular-seed project.

The game is meant to be played on multiple screens with your friends in your living room. One screen (Your TV) shows the game to everyone watching, and then there is one screen (smartphone/tablet) per player. All of these devices merely need to run a modern browser (need to support WebSockets).

### To Run

Start a webserver, hosting all files in the app/ folder.
Start the NodeJS server (node server/server.js)
Go to your webserver using your favorite modern browser.  (If your webserver and NodeJS are on separate machines, you will have to update the host defined in services.js)

### License

MIT. See MIT-LICENSE.txt